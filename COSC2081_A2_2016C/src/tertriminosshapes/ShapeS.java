package tertriminosshapes;

/**
 * Created by PC on 12/6/2016.
 */
public class ShapeS extends Tetrimino {

    public ShapeS() {
        super(new boolean[][][]{
                {
                        {false,true,true},
                        {true,true,false}
                },
                {
                        {true,false},
                        {true,true},
                        {false,true}
                },
                {
                        {false,true,true},
                        {true,true,false}
                }
                ,
                {
                        {true,false},
                        {true,true},
                        {false,true}
                }
        }, Tetrimino.Tetriminos.S);
    }
}
