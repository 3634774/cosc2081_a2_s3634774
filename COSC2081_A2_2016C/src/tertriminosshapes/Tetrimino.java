package tertriminosshapes;

import javax.swing.*;

/**
 * Created by PC on 12/6/2016.
 */

public abstract class Tetrimino {
    private boolean[][][] models;   //will contain the 4 models for each rotation
    private int modelIndex;      //current set model
    private Tetriminos shape;

    public enum Tetriminos {
        I, J, L, O, S, T, Z;
    };

    public Tetrimino(boolean[][][] models, Tetriminos shape) {
        modelIndex = 0;
        this.models = models;
        this.shape = shape;
    }

    /**
     *
     * @param orientation   - Takes an int between [0,3]
     * @return
     */
    public boolean[][] model(int orientation) {
        return models[orientation];
    }

    public void setCurrentModel(int orientation) {
        modelIndex = orientation;
    }

    public int getCurrentModel() {
        return modelIndex;
    }

    public boolean[][] getShape() {
        return models[modelIndex];
    }

    public int getWidth(int modelIndex) {
        return models[modelIndex][0].length;
    }

    public int getHeight(int modelIndex) {
        return models[modelIndex].length;
    }

    /*
    public int getWidth(int modelIndex) {
        boolean[] projection = new boolean[models[modelIndex][0].length];
        for (int x=0; x<models[modelIndex].length; x++) {
            for (int y=0; y<models[modelIndex][x].length; y++) {
                if (models[modelIndex][x][y])
                    projection[y] = true;
            }
        }
        int width = 0;
        for (boolean cell : projection)
            if (cell)
                width++;
        return width;
    }

    public int getHeight(int modelIndex) {
        boolean[] projection = new boolean[models[modelIndex].length];
        for (int x=0; x<models[modelIndex].length; x++) {
            for (int y=0; y<models[modelIndex][x].length; y++) {
                if (models[modelIndex][x][y]) {
                    projection[x] = true;
                    break;
                }
            }
        }
        int height = 0;
        for (boolean cell : projection)
            if (cell)
                height++;
        return height;
    }

    */
    // debugging purposes
    public static void printConsole(boolean[][] shape, char printChar) {
        int width = shape.length,
                height = shape[0].length;
        boolean slot;
        for (int i=0; i<width; i++) {
            for (int j=0; j<height; j++) {
                slot = shape[i][j];
                if (slot)
                    System.err.print(printChar);
                else
                    System.err.print(" ");
            }
            System.err.println();
        }
        for (int j=0; j<height; j++){
             for (int i=0; i<width; i++) {
                slot = shape[i][j];
                if (slot)
                    System.err.print(printChar);
                else
                    System.err.print(" ");
            }
            System.err.println();
        }
        for (int i=width-1; i>=0; i--) {
            for (int j=height-1; j>=0; j--) {
                slot = shape[i][j];
                if (slot)
                    System.err.print(printChar);
                else
                    System.err.print(" ");
            }
            System.err.println();
        }
        for (int j=height-1; j>=0; j--) {
            for (int i=width-1; i>=0; i--) {
                slot = shape[i][j];
                if (slot)
                    System.err.print(printChar);
                else
                    System.err.print(" ");
            }
            System.err.println();
        }
    }

    public static void main(String[] args){
        Tetrimino[] shapes = new Tetrimino[]{new ShapeJ()}; //T(), new ShapeS(), new ShapeZ()};
        String[] sNames = new String[]{"I","S","Z"};
        int n = 0;
        for (Tetrimino shape : shapes) {
            System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~"+sNames[n++]+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" +
                    "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            for (int i = 0; i < 1; i++) {
                shape.setCurrentModel(i);
                System.err.println("Current Model # : \t" + shape.getCurrentModel());
                System.err.println("Width : \t\t\t" + shape.getWidth(shape.getCurrentModel()));
                System.err.println("Height : \t\t\t" + shape.getHeight(shape.getCurrentModel()));
                System.err.println("View :");
                printConsole(shape.getShape(), 'M');
                System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            }
        }
    }
}
