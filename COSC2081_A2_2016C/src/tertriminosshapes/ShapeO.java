package tertriminosshapes;

/**
 * Created by PC on 12/6/2016.
 */
public class ShapeO extends Tetrimino {

    public ShapeO() {
        super(new boolean[][][]{
                {
                        {true,true},
                        {true,true}
                },
                {
                        {true,true},
                        {true,true}
                },
                {
                        {true,true},
                        {true,true}
                }
                ,
                {
                        {true,true},
                        {true,true}
                }
        }, Tetrimino.Tetriminos.O);
    }
}
