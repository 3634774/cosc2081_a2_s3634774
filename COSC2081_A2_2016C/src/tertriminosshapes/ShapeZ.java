package tertriminosshapes;

/**
 * Created by PC on 12/6/2016.
 */
public class ShapeZ extends Tetrimino {

    public ShapeZ() {
        super(new boolean[][][]{
                {
                        {true,true,false},
                        {false,true,true}
                },
                {
                        {false,true},
                        {true,true},
                        {true,false}
                },
                {
                        {true,true,false},
                        {false,true,true}
                }
                ,
                {
                        {false,true},
                        {true,true},
                        {true,false}
                }
        }, Tetrimino.Tetriminos.Z);
    }
}
