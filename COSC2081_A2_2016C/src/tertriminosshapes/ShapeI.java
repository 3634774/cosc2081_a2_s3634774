package tertriminosshapes;

/**
 * Created by PC on 12/6/2016.
 */
public class ShapeI extends Tetrimino {

    public ShapeI() {
        super(new boolean[][][]{
                {
                        {true},
                        {true},
                        {true},
                        {true}
                },
                {
                        {true, true, true, true}
                },
                {
                        {true},
                        {true},
                        {true},
                        {true}
                }
                ,
                {
                        {true,true,true,true}
                }
        }, Tetrimino.Tetriminos.I);
    }
}
