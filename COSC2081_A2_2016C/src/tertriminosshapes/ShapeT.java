package tertriminosshapes;

/**
 * Created by PC on 12/6/2016.
 */
public class ShapeT extends Tetrimino {

    public ShapeT() {
        super(new boolean[][][]{
                {
                        {true,true,true},
                        {false,true,false}
                },
                {
                        {true,false},
                        {true,true},
                        {true,false}
                },
                {
                        {false,true,false},
                        {true,true,true}
                }
                ,
                {
                        {false,true},
                        {true,true},
                        {false,true}
                }
        }, Tetrimino.Tetriminos.T);
    }
}
