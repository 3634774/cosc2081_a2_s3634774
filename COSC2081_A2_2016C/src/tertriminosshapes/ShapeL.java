package tertriminosshapes;

/**
 * Created by PC on 12/6/2016.
 */
public class ShapeL extends Tetrimino {

    public ShapeL() {
        super(new boolean[][][]{
                {
                        {true,true,true,true},
                        {true,false,false,false}
                },
                {
                        {true,false},
                        {true,false},
                        {true,false},
                        {true,true}

                },
                {
                        {false,false,false,true},
                        {true,true,true,true}
                }
                ,
                {
                        {true,true},
                        {false,true},
                        {false,true},
                        {false,true}
                }
        }, Tetrimino.Tetriminos.L);
    }
}
