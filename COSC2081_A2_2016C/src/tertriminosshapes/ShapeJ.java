package tertriminosshapes;

/**
 * Created by PC on 12/6/2016.
 */
public class ShapeJ extends Tetrimino {

    public ShapeJ() {
        super(new boolean[][][]{
                {
                        {true,true,true,true},
                        {false,false,false,true}
                },
                {
                        {true,true},
                        {true,false},
                        {true,false},
                        {true,false}
                },
                {
                        {true,false,false,false},
                        {true,true,true,true},
                }
                ,
                {
                        {false,true},
                        {false,true},
                        {false,true},
                        {true,true}
                }
        }, Tetrimino.Tetriminos.J);
    }
}
