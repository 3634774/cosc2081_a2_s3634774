import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.event.*;

import tertriminosshapes.*;

import java.util.Arrays;
import java.util.Random;

import tertriminosshapes.Tetrimino.Tetriminos;

public class GameDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonStart;
    private JButton buttonQuit;
    private JTextArea outputArea;
    private JLabel statusLabel;

    private int x = 0;
    private int y = 0;
    private int dx = 0, dy = 0;         //both represent displacement in 2D space
    private int score;
    private GameThread gameThread;
    private boolean printed;
    private Tetrimino currentPiece;

    private static final char BLOCK = 'X';
    private static final char EMPTY_BLOCK = ' ';
    private static final int NO_SPACE = 0;
    private final Object LOCK = new Object();
    private final int WIDTH, HEIGHT;

    public GameDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonStart);

        buttonStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonQuit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        char[] initString = new char[outputArea.getColumns() * outputArea.getRows()];
        Arrays.fill(initString, ' ');
        outputArea.append(String.valueOf(initString));
        outputArea.addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                processKey(e);
            }
        });

        WIDTH = outputArea.getColumns();
        HEIGHT = outputArea.getRows();

        gameThread = new GameThread();
    }

    private void onOK() {
        gameThread.start();
        buttonStart.setEnabled(false);
        outputArea.requestFocus();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private char getChar(int x, int y) {
        try {
            return outputArea.getText(y * outputArea.getColumns() + x, 1).charAt(0);
        } catch (BadLocationException e) {
            e.printStackTrace();
            return ' ';
        }
    }

    private void setChar(char c, int x, int y) {
        int pos = y * outputArea.getColumns() + x;
        outputArea.replaceRange(Character.toString(c), pos, pos + 1);
    }

    private void setCurrentPiece(Tetrimino t) {
        currentPiece = t;
    }

    private static Tetrimino randomTetrimino() {
        Tetriminos[] all = Tetriminos.values();
        int random = new Random().nextInt(all.length);
        Tetriminos randomShape = all[random];
        switch (randomShape) {
            case I:
                return new ShapeI();
            case J:
                return new ShapeJ();
            case L:
                return new ShapeL();
            case O:
                return new ShapeO();
            case S:
                return new ShapeS();
            case T:
                return new ShapeT();
            case Z:
                return new ShapeZ();
            default:    //code should never reach here anyways so return anything
                return new ShapeI();
        }
    }

    private boolean printTetrimino(int x, int y) {
        char output;
        int model = currentPiece.getCurrentModel();
        for (int i = 0; i < currentPiece.getWidth(model); i++) {
            for (int j = 0; j < currentPiece.getHeight(model); j++) {
                boolean noOverflow = (x + i < outputArea.getColumns() && y + j < outputArea.getRows()),
                        slot = currentPiece.getShape()[j][i];
                if (slot)
                    output = BLOCK;
                else
                    continue;
                if (noOverflow)
                    setChar(output, x + i, y + j);
                else
                    return false;
            }
        }
        return true;   //tetrimino printed successfully
    }

    //remove tetrimino minus the displacement (dx,dy)
    private void removePrint(int x, int y) {
        int model = currentPiece.getCurrentModel();
        for (int i = 0; i < currentPiece.getWidth(model); i++) {
            for (int j = 0; j < currentPiece.getHeight(model); j++) {
                boolean slot = currentPiece.getShape()[j][i],
                        overflow = (x + i >= outputArea.getColumns() || y + j >= outputArea.getRows());
                if (overflow)
                    continue;
                if (slot)
                    setChar(EMPTY_BLOCK, x + i, y + j);
            }
        }
    }

    private boolean reachedBottom() {
        int sHeight = currentPiece.getHeight(currentPiece.getCurrentModel());
        return (y + sHeight == outputArea.getRows());
    }

    private void finishPieceLifecycle() {
        printTetrimino(x,y);        //draw back last tetrimino initially removed in animation cycle
        checkFullLines();

        Tetrimino t = randomTetrimino();
        checkEndGame(t);
        //Set up new tetrimino
        x = outputArea.getColumns() / 2 - 1;
        y = 0;
        setCurrentPiece(t); //generate new tetrimino
    }

    private void checkEndGame(Tetrimino t) {
        if (isDead(t)) {
            try{
                statusLabel.setText("YOU LOST! ..|..,");
                gameThread.join();
                dispose();
            }
            catch(InterruptedException e){}
        }
    }

    private boolean isDead(Tetrimino t) {
        int x = outputArea.getColumns()/2-1,
                y = 0,
                maxX = x + t.getWidth(t.getCurrentModel()) + 1,
                maxY = y + t.getHeight(t.getCurrentModel()) + 1;
        for (; x < maxX; x++)
            for (; y < maxY; y++)
                if (getChar(x,y)==BLOCK)
                    return true;
        return false;
    }

    private static boolean isEmpty(char[] row) {
        for (char c : row)
            if (c != ' ')
                return false;
        return true;
    }

    private static int collisionDistance(char[] row) {
        if (isEmpty(row))
            return row.length;
        int i = 0, c = 0;
        while (i<row.length && row[i]==BLOCK)
            i++;        //skip starting white blocks
        while (i<row.length && row[i]==BLOCK)
            i++;        //skip tetrimino blocks
        while (i<row.length && row[i]==EMPTY_BLOCK) {
            c++;        //start chained white blocks
            i++;
        }
        return c;
    }

    private static char[] reverseRow(char[] subsectionRow) {
        char[] reversed = new char[subsectionRow.length];
        for (int i = subsectionRow.length-1, c = 0; i>=0; i--)
            reversed[c++] = subsectionRow[i];
        return reversed;
    }

    private static int minCollisionDistance(int[] collisionDistances) {
        int min = Integer.MAX_VALUE;
        for (int collisionDistance : collisionDistances) {
            min = Math.min(min, collisionDistance);
            if (collisionDistance==0)   //since by hypothesis all distances are >= 0
                return 0;
        }
        return min;
    }

    //detects nearest row collision distance (CD) to the right or left of tetrimino piece
    private static int nearestRowCollisions(char[][] subsection, boolean right) {
        int[] collisionDistances = new int[subsection.length];
        for (int row=0; row<subsection.length; row++) {
            collisionDistances[row] =
                    collisionDistance((right) ?
                            subsection[row] : reverseRow(subsection[row]));
        }
        return minCollisionDistance(collisionDistances);
    }

    private static char[] getColumn(char[][] subsection, int col) {
        char[] column = new char[subsection.length];
        int c = 0;
        for (int row=0; row<subsection.length; row++)
            column[c++] = subsection[row][col];
        return column;
    }

    //no need to detect collisions from above since tetriminos can only fall
    private static int nearestColumnCollisions(char[][] subsection) {
        int[] collisionDistances = new int[subsection[0].length];
        char[] column;
        for (int col = 0; col<subsection[0].length; col++) {
            column = getColumn(subsection,col);
            collisionDistances[col] = collisionDistance(column);
        }
        return minCollisionDistance(collisionDistances);
    }

    private char[][] subBoard(int xi, int yi, int xf, int yf){
        char[][] subsection = new char[yf-yi][xf-xi];
        for (int x=xi; x<xf; x++)
            for (int y=yi; y<yf; y++)
                subsection[y-yi][x-xi] = getChar(x,y);
        return subsection;
    }

    //Get all rows including the piece till left
    private int detectLeftCollisions(int model) {
        int sWidth = currentPiece.getWidth(model),   //used for collisions
                sHeight = currentPiece.getHeight(model),
                xi = 0,
                yi = y,
                xf = x + sWidth,
                yf = y + sHeight;
        return nearestRowCollisions(subBoard(xi,yi,xf,yf),false);
    }

    //Get all rows including the piece till right
    private int detectRightCollisions(int model) {
        int sHeight = currentPiece.getHeight(model),
                xi = x,
                yi = y,
                xf = WIDTH,
                yf = y + sHeight;
        return nearestRowCollisions(subBoard(xi,yi,xf,yf),true);
    }

    //Get all columns including the piece till the bottom
    private int detectBottomCollisions(int model) {
        int sWidth = currentPiece.getWidth(model),
                xi = x,
                yi = y,
                xf = x + sWidth,
                yf = HEIGHT;
        return nearestColumnCollisions(subBoard(xi, yi, xf, yf));
    }

    private void checkFullLines() {
        for (int row=outputArea.getRows()-1; row>=0; row--)
            if (isFull(row))
                clear(row);
    }

    private boolean isFull(int l) {
        for (int x=0; x<outputArea.getColumns(); x++)
            if (getChar(x,l)==EMPTY_BLOCK)
                return false;
        return true;
    }

    private void clear(int line) {
        char[] above, under;
        int current, next;
        for (int j = line; j>0; j--) {
            current = j;
            next = j-1;
            above = getBoardLine(next);
            under = getBoardLine(current);
            setBoardLine(next,under);
            setBoardLine(current,above);
        }
        char[] emptyLine = new char[outputArea.getColumns()];
        for (int i=0; i<outputArea.getColumns(); i++)
            emptyLine[i] = EMPTY_BLOCK;
        setBoardLine(0,emptyLine);
        score += 1;
    }

    private void setBoardLine(int line, char[] charLine) {
        for (int x=0; x<outputArea.getColumns(); x++)
            setChar(charLine[x], x, line);
    }

    private char[] getBoardLine(int line) {
        char[] charLine = new char[outputArea.getColumns()];
        for (int x=0; x<outputArea.getColumns(); x++)
            charLine[x] = getChar(x,line);
        return charLine;
    }

    private void processKey(KeyEvent e) {
        //https://docs.oracle.com/javase/tutorial/essential/concurrency/locksync.html#MainFlow
        synchronized (LOCK) {
            dx = 0;
            dy = 0;
            int model = currentPiece.getCurrentModel(), //used for rotations
                    nextModel = model,
                    nSWidth,
                    nSHeight,
                    nx = x, ny = y;
            boolean collided = false,                   //to disable piece movement update
                    stopPiece = false;                  //for new piece generation

            //process information to know if we need to update animations/positions or not
            switch (e.getKeyCode()) {
                case KeyEvent.VK_DOWN:
                    if (detectBottomCollisions(model) == NO_SPACE)
                        stopPiece = true;
                    else
                        dy = 1;
                    break;
                case KeyEvent.VK_LEFT:
                    if (detectLeftCollisions(model) == NO_SPACE)
                        collided = true;
                    else
                        dx = -1;
                    break;
                case KeyEvent.VK_RIGHT:
                    if (detectRightCollisions(model) == NO_SPACE)
                        collided = true;
                    else
                        dx = 1;
                    break;
                case KeyEvent.VK_SPACE:
                    dy = detectBottomCollisions(model);
                    break;
                case KeyEvent.VK_Z:
                    nextModel = (++model < 4) ? model : 0;
                    nSWidth = currentPiece.getWidth(nextModel);
                    nSHeight = currentPiece.getHeight(nextModel);
                    if (x + nSWidth > WIDTH)    //check right border overflow
                        nx -= nx + nSWidth - WIDTH;   //remove excess/overflow
                    if (y + nSHeight > HEIGHT) //check bottom border overflow
                        ny -= ny + nSHeight - HEIGHT;
                    break;
                case KeyEvent.VK_X:
                    nextModel = (--model > -1) ? model : 3;
                    nSWidth = currentPiece.getWidth(nextModel);
                    nSHeight = currentPiece.getHeight(nextModel);
                    if (x + nSWidth > WIDTH)
                        nx -= nx + nSWidth - WIDTH;
                    if (y + nSHeight > HEIGHT)
                        ny -= ny + nSHeight - HEIGHT;
                    break;
            }
            if (collided) {
                dx = 0;
                dy = 0;
                nextModel = model;
            }

            //Start animation cycle by removing drawn image
            removePrint(nx, ny);
            //update movement after having processed informatioin
            x = nx + dx;
            y = ny + dy;
            currentPiece.setCurrentModel(nextModel);
            //Finish animation cycle by drawing moved image again
            printTetrimino(x, y);

            if (reachedBottom() || stopPiece)
                finishPieceLifecycle();
        }
    }

    private class GameThread extends Thread {
        private int sleepTime = 1000;

        public void run() {
            int model;
            score = 0;
            setCurrentPiece(randomTetrimino());
            printed = printTetrimino(x,y);
            while (true) {
                try {
                    sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (LOCK) {
                    dy = 1;     //create falling illusion

                    model = currentPiece.getCurrentModel();
                    if (detectBottomCollisions(model)==NO_SPACE)
                        finishPieceLifecycle();     //stop

                    if (printed)                    //animation lifecycle:
                        removePrint(x, y);          //-remove drawing
                    y += dy;                        //-update info
                    printed = printTetrimino(x, y); //-draw again
                }
                statusLabel.setText(""+score);
            }
        }
    }

    public static void main(String[] args) {
        GameDialog dialog = new GameDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
