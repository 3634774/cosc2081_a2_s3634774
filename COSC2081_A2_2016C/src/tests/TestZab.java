package tests;

public class TestZab {

    private static char[][] subBoard(int xi, int yi, int xf, int yf, char[][] board){
        char[][] subsection = new char[xf-xi][yf-yi];
        for (int x=xi; x<xf; x++)
            for (int y=yi; y<yf; y++)
                subsection[x-xi][y-yi] = board[x][y];
        return subsection;
    }

    //debug
    private static void print(char[] row) {
        System.out.print("");
        for (char c : row)
            System.out.print(row);
        System.out.println();
    }

    //debug
    private static void print(char[][] table) {
        String debug = "";
        for (char[] row : table) {
            for (char c : row)
                debug += c;
            debug += "\n";
        }
        System.out.println(debug);
    }

    private static char[] getColumn(char[][] subsection, int col) {
        char[] column = new char[subsection[0].length];
        int c = 0;
        for (int row=0; row<subsection.length; row++)
            column[c++] = subsection[row][col];
        return column;
    }

    public static void main(String[] args) {
        char[][] table = {
                {'0', '1', '2'},
                {'3', '4', '5'},
                {'6', '7', '8'}
        };
        print(subBoard(1,1,3,2,table));
    }
}
