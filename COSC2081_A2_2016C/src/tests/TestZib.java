package tests;

/**
 * Created by PC on 12/10/2016.
 */
public class TestZib {
    private static final char BLOCK = 'X';
    private static final char EMPTY_BLOCK = ' ';

    private static boolean isEmpty(char[] row) {
        for (char c : row)
            if (c != ' ')
                return false;
        return true;
    }


    private static int collisionDistance(char[] row) {
        if (isEmpty(row))
            return row.length;
        int i = 0, c = 0;
        while (i<row.length && row[i]==EMPTY_BLOCK)
            i++;        //skip starting white blocks
        while (i<row.length && row[i]==BLOCK)
            i++;        //skip tetrimino blocks
        while (i<row.length && row[i++]==EMPTY_BLOCK)
            c++;        //start counting distance
        return c;
    }

    public static void main(String[] args) {
        String row = "   ",
                row1 = "XXX XX XX";
        System.out.println(collisionDistance(row.toCharArray()));
    }
}
